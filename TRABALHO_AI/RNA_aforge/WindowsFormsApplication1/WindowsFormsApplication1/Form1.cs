﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge.Controls;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        ActivationNetwork network;
        public Form1()
        {
            InitializeComponent();

            // initialize input and output values
            double[][] input = new double[10][] {
                new double[] {-999999},
                new double[] {-200},
                new double[] {-1567},
                new double[] {-1},
                new double[] {0},
                new double[] {0},
                new double[] {1},
                new double[] {1567},
                new double[] {200},
                new double[] {999999}
};
            double[][] output = new double[10][] {
                new double[] {0},
                new double[] {0},
                new double[] {0},
                new double[] {0},
                new double[] {0.5},
                new double[] {0.5},
                new double[] {1},
                new double[] {1},
                new double[] {1},
                new double[] {1}
};
            // create neural network
            double sigmoidAlphaValue = 2.0;
            network = new ActivationNetwork((IActivationFunction)new SigmoidFunction(sigmoidAlphaValue)
                , 1, 2, 1);

            // create teacher
            BackPropagationLearning teacher = new BackPropagationLearning(network);

            ActivationLayer layer = network[0];


            teacher.LearningRate = 1;
            //teacher.Momentum = 0.0;


            richTextBox1.Clear();

            /*
            richTextBox1.AppendText(//"taxa: " + teacher.LearningRate + "\n" +

            " W0: " + layer[0][0].ToString("F2") +
            " W1: " + layer[1][0].ToString("F2") +
            " W2: " + layer[0][1].ToString("F2") +
            " W3: " + layer[1][1].ToString("F2") +
            " Lim0: " + layer[0].Threshold.ToString("F2") +
            " Lim1: " + layer[1].Threshold.ToString("F2") + "\n"

    );*/

            double error = 100;
            int MAX_interacao = 40, interacao = 0;

            while (interacao < MAX_interacao)
            {
                //while (interacao > MAX_interacao || error > 0.1){

                // run epoch of learning procedure
                error = teacher.RunEpoch(input, output);
                // check error value to see if we need to stop
                // ...

                richTextBox1.AppendText("Iteração: " + interacao + " Erro: " + error.ToString("F2") + "\n");
                interacao++;
            }

            for (int k = 0; k < 10; k++)
            {

                network.Compute(input[k]);

                String resultado = "    SEM RESULTADO";

                if (network.Output[0] < 0.1)
                {
                    resultado = "   NUMERO É NEGATIVO!!!";
                }
                if (network.Output[0] > 0.4 && network.Output[0] < 0.6)
                {
                    resultado = "   NUMERO É NULO!!!";
                }
                if (network.Output[0] > 0.9)
                {
                    resultado = "   NUMERO É POSITIVO!!!";
                }

                richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " Saída: " + network.Output[0].ToString("F2") + resultado);
            }

            richTextBox1.AppendText("\nFIM\n");


        }

        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            String valor = campo_numero.Text;
            double n = Convert.ToDouble(valor);
            double[][] num = new double[1][] {new double[] {n}};

            network.Compute(num[0]);

            String resultado = "    SEM RESULTADO";

            if (network.Output[0] < 0.1)
            {
                resultado = "   NUMERO É NEGATIVO!!!";
            }
            if (network.Output[0] > 0.4 && network.Output[0] < 0.6)
            {
                resultado = "   NUMERO É NULO!!!";
            }
            if (network.Output[0] > 0.9)
            {
                resultado = "   NUMERO É POSITIVO!!!";
            }

            richTextBox1.Clear();
            richTextBox1.AppendText("\n - Entradas :" + n + " Saída: " + network.Output[0].ToString("F2") + resultado);

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
