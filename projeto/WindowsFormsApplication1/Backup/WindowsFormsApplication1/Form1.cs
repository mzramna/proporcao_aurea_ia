﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge.Controls;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {



            // initialize input and output values
            double[][] input = new double[4][] {
    new double[] {0, 0},
    new double[] {0, 1},
    new double[] {1, 0},
    new double[] {1, 1}
};
            double[][] output = new double[4][] {
    new double[] {0},
    new double[] {1},
    new double[] {1},
    new double[] {0}
};
            // create neural network
            double sigmoidAlphaValue = 2.0;
            ActivationNetwork network = new ActivationNetwork(
                (IActivationFunction)new SigmoidFunction(sigmoidAlphaValue),
                2, // two inputs in the network
                2, // two neurons in the first layer
                1); // one neuron in the second layer
            // create teacher
            BackPropagationLearning teacher =
                new BackPropagationLearning(network);

            ActivationLayer layer = network[0];


            teacher.LearningRate = 0.2;
            //teacher.Momentum = 0.0;


            int i = 0;
            int iterations = 5000;
            richTextBox1.Clear();

            /*
            richTextBox1.AppendText(//"taxa: " + teacher.LearningRate + "\n" +

            " W0: " + layer[0][0].ToString("F2") +
            " W1: " + layer[1][0].ToString("F2") +
            " W2: " + layer[0][1].ToString("F2") +
            " W3: " + layer[1][1].ToString("F2") +
            " Lim0: " + layer[0].Threshold.ToString("F2") +
            " Lim1: " + layer[1].Threshold.ToString("F2") + "\n"

    );*/

            double error=100;
            while (i<iterations && error>0.1)
            {
                // run epoch of learning procedure
                error = teacher.RunEpoch(input, output);
                // check error value to see if we need to stop
                // ...

                richTextBox1.AppendText("Iteração: " + i + " Erro: " + error.ToString("F2") + "\n");
                
                i++;
            }

            richTextBox1.AppendText(

                        " W0: " + layer[0][0].ToString("F2") +
                        " W1: " + layer[1][0].ToString("F2") +
                        " W2: " + layer[0][1].ToString("F2") +
                        " W3: " + layer[1][1].ToString("F2") +
                        " Lim0: " + layer[0].Threshold.ToString("F2") +
                        " Lim1: " + layer[1].Threshold.ToString("F2") + "\n"
                
                );

            for (int k = 0; k < 4; k++)
            {
                network.Compute(input[k]);
                //richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " Saída: " + network.Compute(input[k]) + " Saída2: " + network.Output[0]);
                richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " Saída: " + network.Output[0].ToString("F2"));
            }
            

            

        }


    }
}
