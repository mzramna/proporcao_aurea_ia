﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge.Controls;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {



            // initialize input and output values
            double[][] input = new double[20][] {
                new double[] {50,100,150},
                new double[] {100,150,200},
                new double[] {150,200,250},
                new double[] {200,250,300},
                new double[] {250,300,350},
                new double[] {300,350,400},
                new double[] {350,400,450},
                new double[] {400,450,500},
                new double[] {450,500,550},
                new double[] {500,550,600},
                new double[] {550,600,650},
                new double[] {600,650,700},
                new double[] {650,700,750},
                new double[] {700,750,800},
                new double[] {750,800,850},
                new double[] {800,850,900},
                new double[] {850,900,950},
                new double[] {900,950,1000},
                new double[] {950,1000,1050},
                new double[] {1000,1050,1100}
            };
            double[][] output = new double[20][] {
                new double[] {200},
                new double[] {250},
                new double[] {300},
                new double[] {350},
                new double[] {400},
                new double[] {450},
                new double[] {500},
                new double[] {550},
                new double[] {600},
                new double[] {650},
                new double[] {700},
                new double[] {750},
                new double[] {800},
                new double[] {850},
                new double[] {900},
                new double[] {950},
                new double[] {1000},
                new double[] {1050},
                new double[] {1100},
                new double[] {1150}
            };

            //criando matriz de entrada do usuario
            double[] entrada = new double[3] { 0 ,  0 , 0 };
            CriaVetordeEntrada(entrada);


            // create neural network
            double sigmoidAlphaValue = 2.0;
            ActivationNetwork network = new ActivationNetwork(
                (IActivationFunction)new SigmoidFunction(sigmoidAlphaValue), 3, 3, 1);
            BackPropagationLearning teacher =
                new BackPropagationLearning(network);

            ActivationLayer layer = network[0];


            teacher.LearningRate = 0.1;
            //teacher.Momentum = 0.0;


            int i = 0;
            int iterations = 30000;
            richTextBox1.Clear();

            double error=100;
            while (i<iterations && error>0.3)
            {
                // run epoch of learning procedure
                error = teacher.RunEpoch(input, output);
                // check error value to see if we need to stop
                // ...

                richTextBox1.AppendText("Iteração: " + i + " Erro: " + error.ToString("F2") + "\n");
                
                i++;
            }

            richTextBox1.AppendText(

                        " W0: " + layer[0][0].ToString("F2")+
                        " W1: " + layer[1][0].ToString("F2")+
                        " W2: " + layer[2][0].ToString("F2")+
                        " W3: " + layer[0][1].ToString("F2") +
                        " W4: " + layer[1][1].ToString("F2") +
                        " W5: " + layer[2][1].ToString("F2")+
                        " W6: " + layer[0][2].ToString("F2") +
                        " W7: " + layer[1][2].ToString("F2") +
                        " W8: " + layer[2][2].ToString("F2")

                );

            
                network.Compute(entrada);
                //richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " Saída: " + network.Compute(input[k]) + " Saída2: " + network.Output[0]);
                richTextBox1.AppendText("- Entrada1 :" + entrada[0] + " Entrada2: " + entrada[1] +"Entrada 3: "+entrada[2]+ " Saída: " + network.Output[0].ToString("F2"));
            
            void CriaVetordeEntrada(double[] matriz){
                /*
                matriz[0] = Double.Parse(entrada1.Text);
                matriz[1] = Double.Parse(entrada2.Text);
                matriz[2] = Double.Parse(entrada3.Text);
                */
                matriz[0] = 50;
                matriz[1] = 100;
                matriz[2] = 150;

            }

            

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
