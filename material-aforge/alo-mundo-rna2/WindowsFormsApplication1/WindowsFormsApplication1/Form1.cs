﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge.Controls;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // initialize input and output values
            double[][] input = new double[8][] {
    new double[] {0, 0, 0},
    new double[] {0, 0, 1},
    new double[] {0, 1, 0},
    new double[] {0, 1, 1},
    new double[] {1, 0, 0},
    new double[] {1, 0, 1},
    new double[] {1, 1, 0},
    new double[] {1, 1, 1}

};
            double[][] output = new double[8][] {
    new double[] {0,1},
    new double[] {1,1},
    new double[] {1,1},
    new double[] {0,1},
    new double[] {0,-1},
    new double[] {1,-1},
    new double[] {1,-1},
    new double[] {0,-1}
};
            // create neural network
            double sigmoidAlphaValue = 2.0;

            ActivationNetwork network = new ActivationNetwork(
                (IActivationFunction)new BipolarSigmoidFunction(sigmoidAlphaValue),
                3, // two inputs in the network
                3, // two neurons in the first layer
                2); // one neuron in the second layer
            // create teacher
            BackPropagationLearning teacher =
                new BackPropagationLearning(network);

            ActivationLayer layer = network[0];

            teacher.LearningRate = 0.2;
            int iterations = 500;//critério de parada
            double error = 0.2;//critério de parada

            int i = 0;
            richTextBox1.Clear();
            System.Collections.ArrayList errorsList = new System.Collections.ArrayList();

            while (i<iterations && error>0.1)
            {
                // run epoch of learning procedure
                error = teacher.RunEpoch(input, output);
                errorsList.Add(error);
                richTextBox1.AppendText("Iteração: " + i + " Erro: " + error.ToString("F2") + "\n");               
                i++;
            }

            for (int k = 0; k < 8; k++)
            {
                network.Compute(input[k]);
                richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " e " + input[k][2] + " Saída: " + network.Output[0].ToString("F2") + " e " +network.Output[1].ToString("F2"));
            }

            // show error's dynamics
            double[,] errors = new double[errorsList.Count, 2];
            int n;
            
            for (i = 0, n = errorsList.Count; i < n; i++)
            {
                errors[i, 0] = i;
                errors[i, 1] = (double)errorsList[i];
            }
            
            errorChart.RangeX = new DoubleRange(0, errorsList.Count - 1);
            errorChart.UpdateDataSeries("error", errors);
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            errorChart.AddDataSeries("error", Color.Red, Chart.SeriesType.Line, 1);
        }
    }
}
