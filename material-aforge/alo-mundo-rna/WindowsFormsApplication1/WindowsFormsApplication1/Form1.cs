﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using AForge;
using AForge.Neuro;
using AForge.Neuro.Learning;
using AForge.Controls;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {



            // initialize input and output values
            double[][] input = new double[25][] {
                new double[] {1,2},
                new double[] {2,3},
                new double[] {3,5},
                new double[] {5,8},
                new double[] {8,13},
                new double[] {13,21},
                new double[] {21,34},
                new double[] {34,55},
                new double[] {55,89},
                new double[] {89,144},
                new double[] {144,233},
                new double[] {233,377},
                new double[] {377,610},
                new double[] {610,987},
                new double[] {987,1597},
                new double[] {1597,2584},
                new double[] {2584,4181},
                new double[] {4181,6765},
                new double[] {6765,10946},
                new double[] {10946,17711},
                new double[] {17711,28657},
                new double[] {28657,46368},
                new double[] {46368,75025},
                new double[] {75025,121393},
                new double[] {121393,196418}
};
            double[][] output = new double[25][] {
                new double[] {2},
                new double[] {3},
                new double[] {5},
                new double[] {8},
                new double[] {13},
                new double[] {21},
                new double[] {34},
                new double[] {55},
                new double[] {89},
                new double[] {144},
                new double[] {233},
                new double[] {377},
                new double[] {610},
                new double[] {987},
                new double[] {1597},
                new double[] {2584},
                new double[] {4181},
                new double[] {6765},
                new double[] {10946},
                new double[] {17711},
                new double[] {28657},
                new double[] {46368},
                new double[] {75025},
                new double[] {121393},
                new double[] {196418}
};

            //criando matriz de entrada do usuario
            double[] entrada = new double[3] { 0, 0, 0 };
            CriaVetordeEntrada(entrada);


            // create neural network
            double sigmoidAlphaValue = 2.0;
            ActivationNetwork network = new ActivationNetwork(
                (IActivationFunction)new SigmoidFunction(sigmoidAlphaValue), 2, 3, 1); 
            BackPropagationLearning teacher =
                new BackPropagationLearning(network);

            ActivationLayer layer = network[0];


            teacher.LearningRate = 0.2;
            //teacher.Momentum = 0.0;


            int i = 0;
            int iterations = 5000;
            richTextBox1.Clear();

            double error=100;
            while (i<iterations && error>0.1)
            {
                // run epoch of learning procedure
                error = teacher.RunEpoch(input, output);
                // check error value to see if we need to stop
                // ...

                richTextBox1.AppendText("Iteração: " + i + " Erro: " + error.ToString("F2") + "\n");
                
                i++;
            }

            richTextBox1.AppendText(

                        " W0: " + layer[0][0].ToString("F2") 
                
                );

            for (int k = 0; k < 3; k++)
            {
<<<<<<< HEAD
                network.Compute(entrada[k]);
                //richTextBox1.AppendText("\n" + k + "- Entrada 1:" + entrada[0] + " Entrada 2:" + ntrada[1] +"Entrada 3: "+ entrada[2] + " Saída: " + network.Compute(input[k]) + " Saída2: " + network.Output[0]);
                richTextBox1.AppendText("\n" + k + "- Entrada 1:" + entrada[0] + " Entrada 2:" + entrada[1] +"Entrada 3:"+ entrada[2] + " Saída: " + network.Output[k].ToString("F2"));
=======
                network.Compute(input[k]);
                //richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " Saída: " + network.Compute(input[k]) + " Saída2: " + network.Output[0]);
                richTextBox1.AppendText("\n" + k + "- Entradas :" + input[k][0] + " e " + input[k][1] + " Saída: " + network.Output[k].ToString("F2"));
>>>>>>> origin
            }
            void CriaVetordeEntrada(double[] matriz){
                
                    matriz[0] = Double.Parse(entrada1.Text);
                    matriz[1] = Double.Parse(entrada2.Text);
                    matriz[2] = Double.Parse(entrada3.Text);
            }

            

        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
